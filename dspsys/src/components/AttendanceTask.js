
import React from 'react';
import './Dashboard.css';
import { Form, Icon, Col, Row, Drawer, Input, Button, Select, Radio, Table, Divider, Tag, TimePicker, InputNumber, TreeSelect, Switch, Slider, Upload, DatePicker, Breadcrumb, Layout, Menu, message, Spin } from 'antd';
import config from '../config'
import moment from 'moment';

const request = require('request')


const FormItem = Form.Item;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


class AttendanceTask extends React.Component {

    constructor() {
        super();
        let data = {
            "odr": "userLogin",
            "code": "033biOG80ZFcvF1qwaF80K3rG80biOGQ",
            "grant_type": "grant_type",
            "mode": "debug"

        }
        const PostTarget = {
            url: config.url + "/scrtmgr/",
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json",
                "cookie": ""
            },
            body: data
        }

        console.log(PostTarget)
        //登录
        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    // alert(body.msg)
                    setTimeout(() => {//[500,1500]
                        message.success('登录成功');

                    }, 500 + 1000 * Math.random());
                    PostTarget.headers.cookie = body.PHPSESSID
                    this.setState({ cookie: body.PHPSESSID })


                    data = {
                        "odr": "userAllBasicInfoGet"
                    }

                    PostTarget.body = data


                    request(PostTarget, (err, res, body) => {
                        if (!err && res.statusCode == 200) {
                            console.log(body) // 请求成功的处理逻辑
                            if (body.errCode == 0) {
                                // alert(body.msg)
                                setTimeout(() => {//[500,1500]
                                    message.success('操作成功');

                                }, 500 + 1000 * Math.random());
                                this.state.userBasicInfo = body.data


                                const node = {
                                    title: '',
                                    value: '',
                                    key: '',
                                    search: '',
                                    children: []
                                }
                                const tree = []
                                for (const key in body.data) {
                                    if (body.data.hasOwnProperty(key)) {
                                        const element = body.data[key];
                                        node.title = element.username
                                        node.value = element.user_id
                                        node.search = element.username
                                        node.key = element.username
                                        tree.push(JSON.parse(JSON.stringify(node)))
                                    }
                                }
                                this.setState({ userInfoTree: tree })
                                console.log(this.state)

                            } else {
                                setTimeout(() => {
                                    this.setState({ loading: false })
                                    message.error('操作失败')
                                }, 500 + 2000 * Math.random());

                            }

                        } else {
                            console.log(err)
                        }
                    })


                    PostTarget.body = {
                        "odr": "locationGet"
                    }


                    request(PostTarget, (err, res, body) => {
                        if (!err && res.statusCode == 200) {
                            console.log(body) // 请求成功的处理逻辑
                            if (body.errCode == 0) {
                                // alert(body.msg)
                                setTimeout(() => {//[500,1500]
                                    message.success('操作成功');

                                }, 500 + 1000 * Math.random());



                                const node = {
                                    title: '',
                                    value: '',
                                    key: '',
                                    search: '',
                                    children: []
                                }
                                const tree = body.data
                                for (const key in tree) {


                                    tree[key]['title'] = tree[key].name
                                    tree[key]['value'] = tree[key].location_id
                                    tree[key]['search'] = tree[key].name
                                    tree[key]['key'] = tree[key].location_id

                                }
                                this.setState({ locationTree: tree })
                                console.log(this.state)

                            } else {
                                setTimeout(() => {
                                    this.setState({ loading: false })
                                    message.error('操作失败')
                                }, 500 + 2000 * Math.random());

                            }

                        } else {
                            console.log(err)
                        }
                    })


                    PostTarget.body = {
                        "odr": "attendanceTaskGet"

                    }

                    request(PostTarget, (err, res, body) => {
                        if (!err && res.statusCode == 200) {
                            console.log(body) // 请求成功的处理逻辑
                            if (body.errCode == 0) {
                                // alert(body.msg)
                                setTimeout(() => {//[500,1500]
                                    message.success('操作成功');

                                }, 500 + 1000 * Math.random());
                                this.state.task = body.data

                                let taskItem = {
                                    key: '',
                                    task_id: '',
                                    name: '',
                                    person: [],
                                    time: '',
                                }

                                let taskList = []

                                for (const key in body.data) {
                                    if (body.data.hasOwnProperty(key)) {
                                        const e = body.data[key];

                                        let flag = true
                                        for (const key in taskList) {
                                            if (taskList.hasOwnProperty(key)) {
                                                const element = taskList[key];
                                                if (element.task_id == e.task_id) {
                                                    taskList[key].person.push(e.username)
                                                    flag = false
                                                }
                                            }
                                        }
                                        if (flag) {
                                            taskItem.key = e.task_id
                                            taskItem.task_id = e.task_id
                                            taskItem.name = e.task_name
                                            taskItem.person = [e.username]
                                            taskItem.time = e.add_time
                                            taskList.push(JSON.parse(JSON.stringify(taskItem)))
                                        }
                                    }
                                }

                                this.setState({ taskData: taskList })
                                console.log(this.state)

                            } else {
                                setTimeout(() => {
                                    this.setState({ loading: false })
                                    message.error('操作失败')
                                }, 500 + 2000 * Math.random());

                            }

                        } else {
                            console.log(err)
                        }
                    })


















                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('登录失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })





    }

    state = {
        cookie: "",
        submitting: false,
        id_validateStatus: "",
        userBasicInfo: [],
        userInfoTree: [],
        locationTree: [],
        taskData: [
            {
                key: '1',
                task_id: 'wdfedfdsf',
                name: 'John Brown',
                person: ["张思成"],
                time: '20:00',
            },
        ],
        DrawerVisible: false,
        DrawerInfo: {},
        taskInfoData: [
            {
                key: '2',
                task_info_id: 'vdfvfd',
                name: "fdf",
                start_time: "213"
            }
        ],

        add_task_location: "",
        add_task_time: "",
        taskUserInfoData: []
    }

    columns = [
        {
            title: '任务编号',
            dataIndex: 'task_id',
            key: 'task_id',
            render: text => <a href="javascript:;">{text}</a>,
        },
        {
            title: '任务名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '人员',
            dataIndex: 'person',
            key: 'person',
            render: tags => (
                <span>
                    {tags.map(tag => {
                        let color = 'geekblue'

                        return (
                            <Tag color={color} key={tag}>
                                {tag}
                            </Tag>
                        );
                    })}
                </span>
            ),
        },
        {
            title: '添加时间',
            key: 'time',
            dataIndex: 'time',

        },
        {
            title: '操作',
            key: 'action',
            render: (text, item) => (
                <span>
                    <a href="javascript:;" onClick={() => this.showDrawer(item)}>详细信息</a>
                    <Divider type="vertical" />
                    <a href="javascript:;">删除</a>
                </span>
            ),
        },
    ]

    task_info_columns = [
        {
            title: '子任务编号',
            dataIndex: 'task_info_id',
            key: 'task_info_id',
            render: text => <a href="javascript:;">{text}</a>,
        },
        {
            title: '地点',
            dataIndex: 'name',
            key: 'name',
            render: text => <a href="javascript:;">{text}</a>,
        },
        {
            title: '时间',
            key: 'start_time',
            dataIndex: 'start_time',
        },
        {
            title: '操作',
            key: 'action',
            render: (text, item) => (
                <span>
                    <a href="javascript:;" onClick={() => this.showDrawer(item)}>详细信息</a>
                    <Divider type="vertical" />
                    <a href="javascript:;">删除</a>
                </span>
            ),
        },
    ]

    person_info_columns = [
        {
            title: '编号',
            dataIndex: 'user_id',
            key: 'user_id',
            render: text => <a href="javascript:;">{text}</a>,
        },
        {
            title: '姓名',
            dataIndex: 'username',
            key: 'username',
            render: text => <a href="javascript:;">{text}</a>,
        },
        {
            title: '操作',
            key: 'action',
            render: (text, item) => (
                <span>
                    <a href="javascript:;">删除</a>
                </span>
            ),
        },
    ]


    showDrawer = (item) => {
        console.log(item)
        this.setState({
            DrawerVisible: true,
            DrawerInfo: JSON.parse(JSON.stringify(item))

        });
        let data = {
            "odr": "attendanceTaskInfoGet",
            "task_id": item.task_id
        }
        const PostTarget = {
            url: config.url + "/scrtmgr/",
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json",
                "cookie": this.state.cookie
            },
            body: data
        }

        console.log(PostTarget)
        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    // alert(body.msg)

                    let task_info_list = body.data
                    for (const key in task_info_list) {
                        if (task_info_list.hasOwnProperty(key)) {
                            task_info_list[key]['key'] = task_info_list[key]['task_info_id']

                        }
                    }

                    this.setState({ taskInfoData: JSON.parse(JSON.stringify(task_info_list)) })




                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('操作失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })




        PostTarget.body = {
            "odr": "attendanceTaskGet"
        }
        console.log(PostTarget)
        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    // alert(body.msg)

                    let taskUserInfoData = []
                    for (const key in body.data) {
                        if (body.data.hasOwnProperty(key)) {

                            if (body.data[key].task_id == this.state.DrawerInfo.task_id && typeof body.data[key].user_id != "undefined") {
                                taskUserInfoData.push(body.data[key])
                                taskUserInfoData[taskUserInfoData.length - 1]["key"] = body.data[key].user_id

                            }


                        }
                    }


                    this.setState({ taskUserInfoData: JSON.parse(JSON.stringify(taskUserInfoData)) })




                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('操作失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })



    };

    closeDrawer = () => {
        this.setState({
            DrawerVisible: false,
        });
    };



    checkProv = (val) => {
        var pattern = /^[1-9][0-9]/;
        var provs = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江 ", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北 ", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏 ", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门" };
        if (pattern.test(val)) {
            if (provs[val]) {
                return true;
            }
        }
        return false;
    }

    checkDate = (val) => {
        var pattern = /^(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)$/;
        if (pattern.test(val)) {
            var year = val.substring(0, 4);
            var month = val.substring(4, 6);
            var date = val.substring(6, 8);
            var date2 = new Date(year + "-" + month + "-" + date);
            if (date2 && date2.getMonth() == (parseInt(month) - 1)) {
                return true;
            }
        }
        return false;
    }

    checkCode = (val) => {
        var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
        var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
        var code = val.substring(17);
        if (p.test(val)) {
            var sum = 0;
            for (var i = 0; i < 17; i++) {
                sum += val[i] * factor[i];
            }
            if (parity[sum % 11] == code.toUpperCase()) {
                return true;
            }
        }
        return false;
    }

    checkID = (val) => {
        if (this.checkCode(val)) {
            var date = val.substring(6, 14);
            if (this.checkDate(date)) {
                if (this.checkProv(val.substring(0, 2))) {
                    return true;
                }
            }
        }
        return false;
    }

    formClear = () => {
        message.info("Clear")

    }

    idOnBlur = (e) => {
        console.log(this.checkID(e.target.value))
        this.setState({ id_validateStatus: "validating" })
        setTimeout((val) => {
            console.log(val)
            if (val == undefined || val == "") {
                this.setState({ id_validateStatus: "" })
            } else {
                if (this.checkID(val)) {
                    this.setState({ id_validateStatus: "success" })
                } else {
                    this.setState({ id_validateStatus: "error" })
                }
            }


        }, 500 + 1500 * Math.random(), e.target.value);

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                let data = {
                    "odr": "archivePersonInfoAdd",
                    "name": values.name,
                    "id_number": values.id_number != "" ? values.id_number : undefined,
                    "birthday": values.birthday == undefined ? undefined : values.birthday.format('YYYY-MM-DD'),
                    "mobilephone": values.mobilephone,
                    "qq": values.qq,
                    "email": values.email,
                    "remarks": values.remarks
                }
                const PostTarget = {
                    url: config.url + "/insight/handler/index.php",
                    method: "POST",
                    json: true,
                    headers: {
                        "content-type": "application/json",
                        "cookie": ""
                    },
                    body: data
                }


                console.log(PostTarget)
                request(PostTarget, (err, res, body) => {
                    if (!err && res.statusCode == 200) {
                        console.log(body) // 请求成功的处理逻辑
                        if (body.errCode == 0) {

                            console.log(body)
                            setTimeout(() => {//[500,1500]
                                message.success('录入成功');
                                setTimeout(() => {//[400,1400]
                                    // window.location.href = config.url + "/"
                                }, 400 + 1000 * Math.random());
                            }, 500 + 1000 * Math.random());



                        } else {
                            setTimeout(() => {
                                this.setState({ loading: false })
                                message.error('登录失败')
                            }, 500 + 2000 * Math.random());

                        }

                    } else {
                        console.log(err)
                    }
                })




                console.log('Received values of form: ', values);
                console.log(data)



            }
        });
    }
    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    }

    inputLocation = (e) => {
        console.log(e)
        this.setState({ add_task_location: e })
    }

    inputTaskTime = (e) => {
        console.log(e.format('HH:mm:ss'))
        this.setState({ add_task_time: e.format('HH:mm:ss') })
    }

    addAttendanceLocation = (e) => {
        let data = {
            "odr": "attendanceTaskInfoAdd",
            "task_id": this.state.DrawerInfo.task_id,
            "location_id": this.state.add_task_location[0],
            "start_time": this.state.add_task_time
        }
        const PostTarget = {
            url: config.url + "/scrtmgr/",
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json",
                "cookie": this.state.cookie
            },
            body: data
        }

        console.log(PostTarget)
        //登录
        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    alert(body.msg)



                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('登录失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })


    }
    input_update_userid = (e) => {
        console.log(e)
    }
    updateUserTask = (e) => {
        this.props.form.validateFields((err, values) => {
            console.log(values)
        })

    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '86',
        })(
            <Select className="icp-selector">
                <Option value="86">+86</Option>
            </Select>
        );


        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };



        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <h1 style={{ marginBottom: 40 }}>考勤任务录入</h1>




                    <FormItem
                        {...formItemLayout}
                        label="姓名"
                        hasFeedback
                    >
                        {getFieldDecorator('name', {
                            rules: [
                                { required: true, message: 'Please enter the name!' },
                            ],
                        })(
                            <TreeSelect
                                showSearch
                                allowClear
                                treeCheckable
                                treeNodeFilterProp="search"
                                style={{ width: 300 }}
                                // value={this.state.value}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                treeData={this.state.userInfoTree}
                                placeholder="Please select"
                                treeDefaultExpandAll
                                onChange={this.onChange}
                            />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="身份证号码"
                        hasFeedback
                        validateStatus={this.state.id_validateStatus}
                        help={this.state.id_validateStatus == "error" ? "身份证校验失败" : null}
                    >
                        {getFieldDecorator('id_number', {
                            rules: [
                                { required: false, message: '请输入合法18位身份证号码(x需要大写)' },
                            ],
                        })(
                            <Input placeholder="请输入身份证号码" onBlur={this.idOnBlur} />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="出生日期"
                    >
                        {getFieldDecorator('birthday', {
                            rules: [{
                                required: false
                            }]
                        })(
                            <DatePicker placeholder="请输入日期" />
                        )}
                    </FormItem>



                    <FormItem
                        wrapperCol={{ span: 12, offset: 6 }}
                    >
                        <Button type="primary" htmlType="submit">提交</Button>
                        <Button size="default" style={{ marginLeft: 20 }} onClick={this.formClear}>清空</Button>
                    </FormItem>


                    <Table columns={this.columns} dataSource={this.state.taskData} />





                </Form>
                <Drawer
                    title="考勤地点列表"
                    width={720}
                    onClose={this.closeDrawer}
                    visible={this.state.DrawerVisible}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Form.Item label="地点">
                            {getFieldDecorator('location', {
                                rules: [{ required: true, message: 'Please select the location' }],
                            })(<TreeSelect
                                showSearch
                                allowClear
                                treeCheckable
                                treeNodeFilterProp="search"
                                style={{ width: 300 }}
                                // value={this.state.value}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                treeData={this.state.locationTree}
                                placeholder="Please select"
                                treeDefaultExpandAll
                                onChange={this.inputLocation}
                            />)}
                        </Form.Item>
                        <FormItem
                            {...formItemLayout}
                            label="时间"
                        >
                            {getFieldDecorator('task_time', {
                                rules: [{
                                    required: true
                                }]
                            })(
                                <TimePicker onChange={this.inputTaskTime} format={"HH:mm"} />
                            )}
                        </FormItem>

                        <Button onClick={this.addAttendanceLocation} type="primary">
                            增加地点
                        </Button>

                    </Form>
                    <Table columns={this.task_info_columns} dataSource={this.state.taskInfoData} />

                    <FormItem
                        {...formItemLayout}
                        label="姓名"
                        hasFeedback
                    >
                        {getFieldDecorator('name', {
                            rules: [
                                { required: true, message: 'Please enter the name!' },
                            ],
                        })(
                            <TreeSelect
                                showSearch
                                allowClear
                                treeCheckable
                                treeNodeFilterProp="search"
                                style={{ width: 300 }}
                                // value={this.state.value}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                treeData={this.state.userInfoTree}
                                placeholder="Please select"
                                treeDefaultExpandAll
                                onBlur={this.input_update_userid}
                            />
                        )}
                    </FormItem>
                    <Button onClick={this.updateUserTask} type="primary">
                        增加人员
                        </Button>
                    <Table columns={this.person_info_columns} dataSource={this.state.taskUserInfoData} />

                    
                    <div
                        style={{
                            position: 'absolute',
                            left: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                            Cancel
                        </Button>
                        <Button onClick={this.onClose} type="primary">
                            Submit
                        </Button>
                    </div>
                </Drawer>

            </div>


        );
    }
}

const attendanceTask = Form.create()(AttendanceTask)

export default attendanceTask