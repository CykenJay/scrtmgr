
import React from 'react';
import './Dashboard.css';
import { Form, Icon, Input, Button, Select, Radio, InputNumber, TreeSelect, Switch, Slider, Upload, DatePicker, Breadcrumb, Layout, Menu, message, Spin } from 'antd';
import config from '../config'
const request = require('request')

const FormItem = Form.Item;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


class BasicInfoAdd extends React.Component {

    constructor() {
        super();
        let data = {
            "odr": "userLogin",
            "code": "033biOG80ZFcvF1qwaF80K3rG80biOGQ",
            "grant_type": "grant_type",
            "mode": "debug"

        }
        const PostTarget = {
            url: config.url + "/scrtmgr/",
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json",
                "cookie": ""
            },
            body: data
        }

        console.log(PostTarget)
        //登录
        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    // alert(body.msg)
                    setTimeout(() => {//[500,1500]
                        message.success('登录成功');

                    }, 500 + 1000 * Math.random());
                    PostTarget.headers.cookie = body.PHPSESSID


                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('登录失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })

        data = {
            "odr": "userAllBasicInfoGet"
        }

        PostTarget.body = data


        request(PostTarget, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log(body) // 请求成功的处理逻辑
                if (body.errCode == 0) {
                    // alert(body.msg)
                    setTimeout(() => {//[500,1500]
                        message.success('操作成功');

                    }, 500 + 1000 * Math.random());
                    this.state.userBasicInfo = body.data


                    const node = {
                        title: '',
                        value: '',
                        key: '',
                        search: '',
                        children: []
                    }
                    const tree = []
                    for (const key in body.data) {
                        if (body.data.hasOwnProperty(key)) {
                            const element = body.data[key];
                            node.title = element.username
                            node.value = element.user_id
                            node.search = element.username
                            node.key = element.username
                            tree.push(JSON.parse(JSON.stringify(node)))
                        }
                    }
                    this.setState({ userInfoTree: tree })
                    console.log(this.state)

                } else {
                    setTimeout(() => {
                        this.setState({ loading: false })
                        message.error('操作失败')
                    }, 500 + 2000 * Math.random());

                }

            } else {
                console.log(err)
            }
        })




    }

    state = {
        submitting: false,
        id_validateStatus: "",
        userBasicInfo: [],
        userInfoTree: [],
        ss: [
            {
                title: 'Node1',
                value: '0-0',
                key: '0-0',
                children: [
                    {
                        title: 'Child Node1',
                        value: '0-0-1',
                        key: '0-0-1',
                    },
                    {
                        title: 'Child Node2',
                        value: '0-0-2',
                        key: '0-0-2',
                    },
                ],
            },
            {
                title: 'Node2',
                value: '0-1',
                key: '0-1',
            },
        ]
    }

    checkProv = (val) => {
        var pattern = /^[1-9][0-9]/;
        var provs = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江 ", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北 ", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏 ", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门" };
        if (pattern.test(val)) {
            if (provs[val]) {
                return true;
            }
        }
        return false;
    }

    checkDate = (val) => {
        var pattern = /^(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)$/;
        if (pattern.test(val)) {
            var year = val.substring(0, 4);
            var month = val.substring(4, 6);
            var date = val.substring(6, 8);
            var date2 = new Date(year + "-" + month + "-" + date);
            if (date2 && date2.getMonth() == (parseInt(month) - 1)) {
                return true;
            }
        }
        return false;
    }

    checkCode = (val) => {
        var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
        var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
        var code = val.substring(17);
        if (p.test(val)) {
            var sum = 0;
            for (var i = 0; i < 17; i++) {
                sum += val[i] * factor[i];
            }
            if (parity[sum % 11] == code.toUpperCase()) {
                return true;
            }
        }
        return false;
    }

    checkID = (val) => {
        if (this.checkCode(val)) {
            var date = val.substring(6, 14);
            if (this.checkDate(date)) {
                if (this.checkProv(val.substring(0, 2))) {
                    return true;
                }
            }
        }
        return false;
    }

    formClear = () => {
        message.info("Clear")

    }

    idOnBlur = (e) => {
        console.log(this.checkID(e.target.value))
        this.setState({ id_validateStatus: "validating" })
        setTimeout((val) => {
            console.log(val)
            if (val == undefined || val == "") {
                this.setState({ id_validateStatus: "" })
            } else {
                if (this.checkID(val)) {
                    this.setState({ id_validateStatus: "success" })
                } else {
                    this.setState({ id_validateStatus: "error" })
                }
            }


        }, 500 + 1500 * Math.random(), e.target.value);

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                let data = {
                    "odr": "archivePersonInfoAdd",
                    "name": values.name,
                    "id_number": values.id_number != "" ? values.id_number : undefined,
                    "birthday": values.birthday == undefined ? undefined : values.birthday.format('YYYY-MM-DD'),
                    "mobilephone": values.mobilephone,
                    "qq": values.qq,
                    "email": values.email,
                    "remarks": values.remarks
                }
                const PostTarget = {
                    url: config.url + "/insight/handler/index.php",
                    method: "POST",
                    json: true,
                    headers: {
                        "content-type": "application/json",
                        "cookie": ""
                    },
                    body: data
                }


                console.log(PostTarget)
                request(PostTarget, (err, res, body) => {
                    if (!err && res.statusCode == 200) {
                        console.log(body) // 请求成功的处理逻辑
                        if (body.errCode == 0) {

                            console.log(body)
                            setTimeout(() => {//[500,1500]
                                message.success('录入成功');
                                setTimeout(() => {//[400,1400]
                                    // window.location.href = config.url + "/"
                                }, 400 + 1000 * Math.random());
                            }, 500 + 1000 * Math.random());



                        } else {
                            setTimeout(() => {
                                this.setState({ loading: false })
                                message.error('登录失败')
                            }, 500 + 2000 * Math.random());

                        }

                    } else {
                        console.log(err)
                    }
                })




                console.log('Received values of form: ', values);
                console.log(data)



            }
        });
    }
    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    }


    tProps = {
        treeData: this.state.userInfoTree,
        value: this.state.value,
        onChange: this.onChange,
        treeCheckable: true,
        showCheckedStrategy: TreeSelect.SHOW_PARENT,
        searchPlaceholder: 'Please select',
        style: {
            width: 300,
        },
    };



    render() {
        const { getFieldDecorator } = this.props.form;
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '86',
        })(
            <Select className="icp-selector">
                <Option value="86">+86</Option>
            </Select>
        );


        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        return (
            <Form onSubmit={this.handleSubmit}>
                <h1 style={{ marginBottom: 40 }}>考勤任务录入</h1>




                <FormItem
                    {...formItemLayout}
                    label="姓名"
                    hasFeedback
                >
                    {getFieldDecorator('name', {
                        rules: [
                            { required: true, message: 'Please enter the name!' },
                        ],
                    })(
                        <TreeSelect
                        showSearch
                        allowClear
                        treeCheckable
                        treeNodeFilterProp="search"
                            style={{ width: 300 }}
                            // value={this.state.value}
                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                            treeData={this.state.userInfoTree}
                            placeholder="Please select"
                            treeDefaultExpandAll
                            onChange={this.onChange}
                        />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="身份证号码"
                    hasFeedback
                    validateStatus={this.state.id_validateStatus}
                    help={this.state.id_validateStatus == "error" ? "身份证校验失败" : null}
                >
                    {getFieldDecorator('id_number', {
                        rules: [
                            { required: false, message: '请输入合法18位身份证号码(x需要大写)' },
                        ],
                    })(
                        <Input placeholder="请输入身份证号码" onBlur={this.idOnBlur} />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="出生日期"
                >
                    {getFieldDecorator('birthday', {
                        rules: [{
                            required: false
                        }]
                    })(
                        <DatePicker placeholder="请输入日期" />
                    )}
                </FormItem>



                <FormItem
                    wrapperCol={{ span: 12, offset: 6 }}
                >
                    <Button type="primary" htmlType="submit">提交</Button>
                    <Button size="default" style={{ marginLeft: 20 }} onClick={this.formClear}>清空</Button>
                </FormItem>
            </Form>
        );
    }
}

const basicInfoAdd = Form.create()(BasicInfoAdd)

export default basicInfoAdd