import React from 'react';
import './Dashboard.css';
import { Form, Icon, Input, Button, Select, Radio, InputNumber, Switch, Slider, Upload, Breadcrumb, Layout, Menu, message, Spin } from 'antd';
import config from '../config'
import AttendanceTask from './AttendanceTask'
import PersonManager from './PersonManager'
const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu

const request = require('request')



class Dashboard extends React.Component {
    constructor() {
        super();
        console.log(AttendanceTask)
    }

    state = {
        loading: false,
        content: <div style={{ fontSize: 50, fontWeight: 800, paddingLeft: 30 }}>Welcome to the Security Manager</div>,
        breadcrumb: <Breadcrumb style={{ margin: '12px 0' }}>
            <Breadcrumb.Item>保卫处管理系统</Breadcrumb.Item>
            <Breadcrumb.Item>首页</Breadcrumb.Item>
            {/* <Breadcrumb.Item></Breadcrumb.Item> */}
        </Breadcrumb>,

    }

    handleMenuClick = (e) => {
        // message.info(JSON.stringify(e))
        console.log(e.key)
        switch (e.key) {
            case 'AttendanceTask':
                this.setState({ loading: true })
                setTimeout(() => {
                    this.setState({
                        breadcrumb: <Breadcrumb style={{ margin: '12px 0' }}>
                            <Breadcrumb.Item>保卫处管理系统</Breadcrumb.Item>
                            <Breadcrumb.Item>基础数据操作</Breadcrumb.Item>
                            <Breadcrumb.Item>考勤任务</Breadcrumb.Item>
                        </Breadcrumb>
                    })
                    this.setState({ content: <AttendanceTask /> })
                    this.setState({ loading: false })
                }, 400 + 1000 * Math.random());
                break;
            case 'PersonManager':
                    this.setState({ loading: true })
                    setTimeout(() => {
                        this.setState({
                            breadcrumb: <Breadcrumb style={{ margin: '12px 0' }}>
                                <Breadcrumb.Item>保卫处管理系统</Breadcrumb.Item>
                                <Breadcrumb.Item>基础数据操作</Breadcrumb.Item>
                                <Breadcrumb.Item>人员管理</Breadcrumb.Item>
                            </Breadcrumb>
                        })
                        this.setState({ content: <PersonManager /> })
                        this.setState({ loading: false })
                    }, 400 + 1000 * Math.random());
                    break;

            default:
                break;
        }
    }

    render() {
        return (

            <Layout style={{ height: '100%' }}>
                <Header className="header">
                    <div className="logo" />
                    <Menu
                        theme="light"
                        mode="horizontal"
                        defaultSelectedKeys={['2']}
                        style={{ lineHeight: '64px' }}
                    >
                        {/* <Menu.Item key="1">nav 1</Menu.Item>
                        <Menu.Item key="2">nav 2</Menu.Item>
                        <Menu.Item key="3">nav 3</Menu.Item> */}
                    </Menu>
                </Header>
                <Layout style={{ height: '100%' }}>
                    <Sider width={250} style={{ background: '#fff', position: 'fixed', top: '66px', bottom: '0px', overflowX: 'hidden', overflowY: 'auto' }}>
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['AttendanceTask']}
                            defaultOpenKeys={['sub1']}
                            style={{ height: '100%' }}
                        >
                            <SubMenu key="sub1" title={<span><Icon type="user" />基础数据操作</span>}>
                                <Menu.Item key="AttendanceTask" onClick={this.handleMenuClick}>新增任务</Menu.Item>
                                <Menu.Item key="PersonManager" onClick={this.handleMenuClick}>人员管理</Menu.Item>
                                {/* <Menu.Item key="3">option3</Menu.Item>
                                <Menu.Item key="4">option4</Menu.Item> */}
                            </SubMenu>
                            {/* <SubMenu key="sub2" title={<span><Icon type="laptop" />subnav 2</span>}>
                                <Menu.Item key="5">option5</Menu.Item>
                                <Menu.Item key="6">option6</Menu.Item>
                                <Menu.Item key="7">option7</Menu.Item>
                                <Menu.Item key="8">option8</Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub3" title={<span><Icon type="notification" />subnav 3</span>}>
                                <Menu.Item key="9">option9</Menu.Item>
                                <Menu.Item key="10">option10</Menu.Item>
                                <Menu.Item key="11">option11</Menu.Item>
                                <Menu.Item key="12">option12</Menu.Item>
                            </SubMenu> */}
                        </Menu>
                    </Sider>
                    <Layout style={{ padding: '0 24px 24px', paddingLeft: 250 + 24 }}>
                        {this.state.breadcrumb}

                        <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280, overflowY: 'auto' }}>
                            <Spin spinning={this.state.loading}>
                                {this.state.content}
                            </Spin>
                        </Content>

                    </Layout>
                </Layout>
            </Layout>
        );
    }
}

// const Dashboard = Form.create()(Dashboard);

export default Dashboard;