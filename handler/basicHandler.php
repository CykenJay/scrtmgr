<?php

function userAuth()
{

    global $db;
    global $_REQ;
    global $_RES;

    if ($db->has('scrtmgr_userinfo', [
        'AND' => [
            'id_number' => $_REQ['id_number'],
            'username' => $_REQ['username'],
            'enable' => 1
        ]
    ])) {


        //生产一个合法id
        do {
            $id = generateId();
        } while ($db->has('scrtmgr_userinfo', [
            'user_id' => $id
        ]));

        //更新user_id与open_id
        $db->update('scrtmgr_userinfo', [
            'user_id' => $id,
            'open_id' => $_SESSION['openid']
        ], [
            'id_number' => $_REQ['id_number'],
            'username' => $_REQ['username'],
            'enable' => 1
        ]);
        $_SESSION['user_id'] = $id;
        $_SESSION['Auth'] = true;
        $_RES['msg'] = 'userAuth Successfully.';
    } else {
        $_SESSION['Auth'] = false;
        $_RES['errCode'] = 4011;
        $_RES['msg'] = 'userAuth Failed';
    }
}

function userBasicInfoAdd()
{
    global $db;
    global $_REQ;
    global $_RES;
    //生产一个合法id
    do {
        $id = generateId();
    } while ($db->has('scrtmgr_userinfo', [
        'user_id' => $id
    ]));

    //插入数据
    $result = $db->insert('scrtmgr_userinfo', [
        'user_id' => $id,
        'username' => $_REQ['username'],
        'telephoneNumber' => $_REQ['telephoneNumber'],
        'gender' => $_REQ['gender'],
        'level' => $_REQ['level'],
        'id_number' => $_REQ['id_number'],
        'mz' => $_REQ['mz'],
        'whcd' => $_REQ['whcd'],
        'hjdxz' => $_REQ['hjdxz'],
        'xzzxz' => $_REQ['xzzxz'],
        'zzmm' => $_REQ['zzmm'],
        'gzbm' => $_REQ['gzbm'],
        'baygw' => $_REQ['baygw'],
        'xzw' => $_REQ['xzw'],
        'rzrq' => $_REQ['rzrq'],
        'bz' => $_REQ['bz'],
        'rzrq' => $_REQ['rzrq'],
        'enable' => 1
    ]);


    //检测是否成功
    if ($db->has('scrtmgr_userinfo', [
        'user_id' => $id
    ])) {
        $_RES['msg'] = 'Add User Successfully.';
    } else {
        $_RES['errCode'] = 4031;
        $_RES['msg'] = 'Add User Failed.';
    }
}

function userBasicInfoGet()
{
    global $db;
    global $_REQ;
    global $_RES;

    if (!isset($_REQ['user_id'])) {
        $_RES['errCode'] = 4023;
        $_RES['msg'] = '请求参数错误';
        return;
    }


    if ($_REQ['user_id'] == '*') {
        $result = $db->select('scrtmgr_userinfo', '*', [
            'level[<=]' => $_SESSION['level']
        ]);
    } else {
        $result = $db->select('scrtmgr_userinfo', '*', [
            'user_id' => $_REQ['user_id'],
            'level[<=]' => $_SESSION['level']
        ]);
    }

    $_RES['data'] = $result;
}



function userBasicInfoUpdate()
{ }

function locationAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    //生产一个合法id
    do {
        $id = generateId();
    } while ($db->has('scrtmgr_location', [
        'location_id' => $id
    ]));

    //插入数据
    $result = $db->insert('scrtmgr_location', [
        'user_id' => $_SESSION['user_id'],
        'location_id' => $id,
        'name' => $_REQ['name'],
        'lon' => $_REQ['lon'],
        'lat' => $_REQ['lat'],
        'enable' => 1
    ]);


    //检测是否成功
    if ($db->has('scrtmgr_location', [
        'location_id' => $id
    ])) {
        $_RES['msg'] = 'Add Location Successfully.';
    } else {
        $_RES['errCode'] = 4031;
        $_RES['msg'] = 'Add Location Failed.';
    }
}


function locationGet()
{
    global $db;
    global $_REQ;
    global $_RES;

    $result = $db->select('scrtmgr_location', [
        'user_id',
        'location_id',
        'name',
        'lon',
        'lat'
    ], [
        'enable' => 1
    ]);

    $_RES['data'] = $result;
    $_RES['msg'] = 'Get location info successfully.';
}



function groupAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    //生产一个合法id
    do {
        $id = generateId();
    } while ($db->has('scrtmgr_group_list', [
        'group_id' => $id
    ]));

    $db->insert('scrtmgr_group_list', [
        'group_id' => $id,
        'name' => $_REQ['name'],
        'description' => $_REQ['description'],
        'enable' => 1
    ]);

    //检测是否成功
    if ($db->has('scrtmgr_group_list', [
        'group_id' => $id
    ])) {
        $_RES['msg'] = 'Add group successfully.';
    } else {
        $_RES['errCode'] = 4041;
        $_RES['msg'] = 'Add group failed.';
    }
}

function groupUserAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    $db->insert('scrtmgr_group_info', [
        'group_id' => $_REQ['group_id'],
        'user_id' => $_REQ['user_id'],
        'enable' => 1
    ]);

    //检测是否成功
    if ($db->has('scrtmgr_group_info', [
        'group_id' => $_REQ['group_id'],
        'user_id' => $_REQ['user_id'],
        'enable' => 1
    ])) {
        $_RES['msg'] = 'Add group_user successfully.';
    } else {
        $_RES['errCode'] = 4042;
        $_RES['msg'] = 'Add group_user failed.';
    }
}

//新建任务
function attendanceTaskAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    //生产一个合法id
    do {
        $id = generateId();
    } while ($db->has('scrtmgr_attendance_task_list', [
        'task_id' => $id
    ]));

    $db->insert('scrtmgr_attendance_task_list', [
        'task_id' => $id,
        'task_name' => $_REQ['task_name'],
        'add_user_id' => $_SESSION['user_id'],
        'add_time' => date('Y-m-d H:i:s', time()),
        'enable' => 1
    ]);

    //检测是否成功
    if ($db->has('scrtmgr_attendance_task_list', [
        'task_id' => $id,
    ])) {
        $_RES['msg'] = 'Add task successfully.';
    } else {
        $_RES['errCode'] = 4051;
        $_RES['msg'] = 'Add task failed.';
    }
}

//获取所有任务列表
function attendanceTaskGet()
{
    global $db;
    global $_REQ;
    global $_RES;

    $result = $db->select('scrtmgr_attendance_task_list', '*', [
        'enable' => 1
    ]);

    $result = $db->select('scrtmgr_attendance_task_list', [
        '[>]scrtmgr_userinfo' => ['task_id'],
    ], [
        'scrtmgr_userinfo.user_id',
        'scrtmgr_userinfo.username',
        'scrtmgr_userinfo.level',
        'scrtmgr_attendance_task_list.task_id',
        'scrtmgr_attendance_task_list.task_name',
        'scrtmgr_attendance_task_list.add_time',
    ], [
        'scrtmgr_attendance_task_list.enable' => 1,
        // 'scrtmgr_userinfo.enable' => 1
    ]);
    $_RES['data'] = $result;
}


//在某任务中新增考勤信息
function attendanceTaskInfoAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    //生产一个合法id
    do {
        $id = generateId();
    } while ($db->has('scrtmgr_attendance_task_info', [
        'task_info_id' => $id
    ]));

    $db->insert('scrtmgr_attendance_task_info', [
        'task_info_id' => $id,
        'task_id' => $_REQ['task_id'],
        'location_id' => $_REQ['location_id'],
        'delta_t' => $_REQ['delta_t'],
        'start_time' => date('H:i:s', strtotime($_REQ['start_time'])),
        'add_user_id' => $_SESSION['user_id'],
        'add_time' => date('Y-m-d H:i:s', time()),
        'enable' => 1
    ]);

    //检测是否成功
    if ($db->has('scrtmgr_attendance_task_info', [
        'task_info_id' => $id,
    ])) {
        $_RES['msg'] = 'Add task successfully.';
    } else {
        $_RES['errCode'] = 4051;
        $_RES['msg'] = 'Add task failed.';
    }
}


//获取考勤任务地点
function attendanceTaskInfoGet()
{
    global $db;
    global $_REQ;
    global $_RES;
    if (isset($_REQ['user_id'])) {
        $result = $db->select('scrtmgr_attendance_task_info', [
            '[>]scrtmgr_location' => 'location_id',
            '[>]scrtmgr_userinfo' => ['task_id'],
            '[>]scrtmgr_attendance_task_list' => ['scrtmgr_userinfo.task_id' => 'task_id']
        ], [
            'scrtmgr_userinfo.user_id',
            'scrtmgr_userinfo.username',
            'scrtmgr_userinfo.level',
            'scrtmgr_location.location_id',
            'scrtmgr_location.name',
            'scrtmgr_location.lon',
            'scrtmgr_location.lat',
            'scrtmgr_attendance_task_list.task_id',
            'scrtmgr_attendance_task_list.task_name',
            'scrtmgr_attendance_task_info.task_info_id',
            'scrtmgr_attendance_task_info.start_time',
            'scrtmgr_attendance_task_info.delta_t'
        ], [
            'scrtmgr_userinfo.user_id' => $_REQ['user_id']
        ]);
    } else if (isset($_REQ['task_id'])) {
        $result = $db->select('scrtmgr_attendance_task_info', [
            '[>]scrtmgr_location' => 'location_id',
            // '[>]scrtmgr_userinfo' => ['task_id'],
            '[>]scrtmgr_attendance_task_list' => ['task_id']
        ], [
            // 'scrtmgr_userinfo.user_id',
            // 'scrtmgr_userinfo.username',
            // 'scrtmgr_userinfo.level',
            'scrtmgr_location.location_id',
            'scrtmgr_location.name',
            'scrtmgr_location.lon',
            'scrtmgr_location.lat',
            'scrtmgr_attendance_task_list.task_id',
            'scrtmgr_attendance_task_list.task_name',
            'scrtmgr_attendance_task_info.task_info_id',
            'scrtmgr_attendance_task_info.start_time',
            'scrtmgr_attendance_task_info.delta_t'
        ], [
            'scrtmgr_attendance_task_info.task_id' => $_REQ['task_id']
        ]);
    } else {
        $result = $db->select('scrtmgr_attendance_task_info', [
            '[>]scrtmgr_location' => 'location_id',
            '[>]scrtmgr_userinfo' => ['task_id'],
            '[>]scrtmgr_attendance_task_list' => ['scrtmgr_userinfo.task_id' => 'task_id']
        ], [
            'scrtmgr_userinfo.user_id',
            'scrtmgr_userinfo.username',
            'scrtmgr_userinfo.level',
            'scrtmgr_location.location_id',
            'scrtmgr_location.name',
            'scrtmgr_location.lon',
            'scrtmgr_location.lat',
            'scrtmgr_attendance_task_list.task_id',
            'scrtmgr_attendance_task_list.task_name',
            'scrtmgr_attendance_task_info.task_info_id',
            'scrtmgr_attendance_task_info.start_time',
            'scrtmgr_attendance_task_info.delta_t'
        ], [
            'scrtmgr_attendance_task_info.enable' => 1
            // 'scrtmgr_attendance_task_info.task_id' => $_REQ['task_id']
        ]);
    }


    $_RES['data'] = $result;
}

//增加考勤记录（尝试考勤）
function attendanceHistoryAdd()
{
    global $db;
    global $_REQ;
    global $_RES;

    //自己签自己
    if ($_REQ['user_id'] == $_SESSION['user_id']) {
        //返回的考勤成功数据
        $res_data = [];
        //获取合法考勤地点
        $result = $db->select('scrtmgr_attendance_task_info', [
            '[>]scrtmgr_location' => 'location_id',
            '[>]scrtmgr_userinfo' => ['task_id'],
            '[>]scrtmgr_attendance_task_list' => ['scrtmgr_userinfo.task_id' => 'task_id']
        ], [
            'scrtmgr_userinfo.user_id',
            'scrtmgr_userinfo.username',
            'scrtmgr_userinfo.level',
            'scrtmgr_location.location_id',
            'scrtmgr_location.name',
            'scrtmgr_location.lon',
            'scrtmgr_location.lat',
            'scrtmgr_attendance_task_list.task_id',
            'scrtmgr_attendance_task_list.task_name',
            'scrtmgr_attendance_task_info.task_info_id',
            'scrtmgr_attendance_task_info.start_time',
            'scrtmgr_attendance_task_info.delta_t'
        ], [
            'AND' => [
                'scrtmgr_userinfo.user_id' => $_REQ['user_id'],

                //待解决：表内数据运算
                // 'OR' => [
                //     'scrtmgr_attendance_task_info.start_time + scrtmgr_attendance_task_info.delta_t[>]' => date('H:i:s', time()),
                //     'scrtmgr_attendance_task_info.start_time - scrtmgr_attendance_task_info.delta_t[<]' => date('H:i:s', time())
                // ],
                // 'scrtmgr_attendance_task_info.start_time[<>]' => ['09:00:00','10:00:00'],

                //判断时间
                // 'scrtmgr_attendance_task_info.start_time[<>]' => [date('H:i:s', time() - ATTENDANCE_TIME_OFFSET), date('H:i:s', time() + ATTENDANCE_TIME_OFFSET)],
                //经度
                // 'scrtmgr_location.lon[<>]' => [$_REQ['lon'] - ATTENDANCE_LOCATION_OFFSET, $_REQ['lon'] + ATTENDANCE_LOCATION_OFFSET],
                //纬度
                // 'scrtmgr_location.lat[<>]' => [$_REQ['lat'] - ATTENDANCE_LOCATION_OFFSET, $_REQ['lat'] + ATTENDANCE_LOCATION_OFFSET],
                'scrtmgr_attendance_task_list.enable' => 1
            ]

        ]);



        foreach ($result as $data) {

            if ($db->has('scrtmgr_attendance_history', [
                'AND' => [
                    'add_time[<>]' => [date('Y-m-d', time()), date('Y-m-d', strtotime('+1 day'))],
                    'task_info_id' => $data['task_info_id'],
                    'user_id' => $data['user_id'],
                    'enable' => 1
                ]
            ])) {
                //若已考勤，则跳过
                continue;
            } else {
                //生产一个合法id
                do {
                    $id = generateId();
                } while ($db->has('scrtmgr_attendance_history', [
                    'history_id' => $id
                ]));

                $db->insert('scrtmgr_attendance_history', [
                    'history_id' => $id,
                    'user_id' => $_REQ['user_id'],
                    'task_info_id' => $data['task_info_id'],
                    'add_user_id' => $_SESSION['user_id'],
                    'add_time' => date('Y-m-d H:i:s', time()),
                    'enable' => 1
                ]);

                //检验是否写入成功
                if ($db->has('scrtmgr_attendance_history', [
                    'history_id' => $id
                ])) {
                    array_push($res_data, $data);
                }
            }
        }

        $_RES['data'] = $res_data;
    } else {
        //代签

        if (!isset($_REQ['user_id'], $_REQ['task_info_id'])) {
            $_RES['errCode'] = 4061;
            $_RES['msg'] = '代签参数错误';
            return;
        }

        // 权限验证，有空再写
        // 1权限 2此任务是否存在以及是否属于该用户
        // $result = $db->select('scrtmgr_userinfo',[
        //     'level'
        // ])

        if ($db->has('scrtmgr_attendance_history', [
            'user_id' => $_REQ['user_id'],
            'task_info_id' => $_REQ['task_info_id'],
            'enable' => 1
        ])) {
            $_RES['errCode'] = 4062;
            $_RES['msg'] = '代签失败，（已签到）。';
            return;
        } else {
            //生产一个合法id
            do {
                $id = generateId();
            } while ($db->has('scrtmgr_attendance_history', [
                'history_id' => $id
            ]));

            $db->insert('scrtmgr_attendance_history', [
                'history_id' => $id,
                'user_id' => $_REQ['user_id'],
                'task_info_id' => $_REQ['task_info_id'],
                'add_user_id' => $_SESSION['user_id'],
                'add_time' => date('Y-m-d H:i:s', time()),
                'enable' => 1
            ]);

            //检测是否成功
            if ($db->has('scrtmgr_attendance_history', [
                'history_id' => $id,
                'enable' => 1
            ])) {
                //代签成功
                $result = $db->select('scrtmgr_attendance_history', [
                    '[>]scrtmgr_attendance_task_info' => 'task_info_id',
                    '[>]scrtmgr_location' => 'location_id',
                    '[>]scrtmgr_userinfo' => ['scrtmgr_attendance_history.user_id' => 'user_id'],
                    '[>]scrtmgr_attendance_task_list' => ['scrtmgr_userinfo.task_id' => 'task_id']
                ], [
                    'scrtmgr_attendance_history.user_id',
                    'scrtmgr_userinfo.username',
                    'scrtmgr_userinfo.level',
                    'scrtmgr_location.location_id',
                    'scrtmgr_location.name',
                    'scrtmgr_location.lon',
                    'scrtmgr_location.lat',
                    'scrtmgr_attendance_task_list.task_id',
                    'scrtmgr_attendance_task_list.task_name',
                    'scrtmgr_attendance_task_info.task_info_id',
                    'scrtmgr_attendance_task_info.start_time',
                    'scrtmgr_attendance_task_info.delta_t'
                ], [
                    'scrtmgr_userinfo.user_id' => $_REQ['user_id'],
                    'scrtmgr_attendance_task_info.task_info_id' => $_REQ['task_info_id'],
                    'scrtmgr_attendance_history.enable' => 1
                ]);

                $_RES['data'] = $result;

                $_RES['msg'] = '代签成功.';
            } else {
                $_RES['errCode'] = 4063;
                $_RES['msg'] = '代签失败';
            }
        }
    }
}


//获取当日已签到列表
function attendanceHistoryGet()
{
    global $db;
    global $_REQ;
    global $_RES;

    //join
    $result = $db->select('scrtmgr_attendance_history', [
        '[>]scrtmgr_attendance_task_info' => ['task_info_id'],
        '[>]scrtmgr_location' => ['location_id'],
        '[>]scrtmgr_userinfo' => ['scrtmgr_attendance_history.user_id' => 'user_id']
    ], [
        'scrtmgr_userinfo.username',
        'scrtmgr_userinfo.task_id',
        'scrtmgr_location.name',
        'scrtmgr_location.lon',
        'scrtmgr_location.lat',
        'scrtmgr_attendance_history.history_id',
        'scrtmgr_attendance_history.user_id',
        'scrtmgr_attendance_history.task_info_id',
        'scrtmgr_attendance_history.add_user_id',
        'scrtmgr_attendance_history.add_time'
    ], [
        'scrtmgr_attendance_history.add_time[<>]' => [date('Y-m-d', time()), date('Y-m-d', strtotime('+1 day'))],
        'scrtmgr_attendance_history.user_id' => $_REQ['user_id'],
        'scrtmgr_attendance_history.enable' => 1
    ]);

    $_RES['data'] = $result;
}




function userAllBasicInfoGet()
{
    global $db;
    global $_REQ;
    global $_RES;

    $result = $db->select('scrtmgr_userinfo', '*', [
        'enable' => 1
    ]);
    $_RES['data'] = $result;
}
