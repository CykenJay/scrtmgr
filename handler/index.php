<?php
$lifeTime = 1 * 3600;
session_set_cookie_params($lifeTime);
session_start();
$_SESSION['last_access'] = time();
$sessid = session_id();
require_once "conn.php";
require_once "config.php";
require_once "utils.php";
require_once "basicHandler.php";



//解析post请求json数据，转化为php对象

$_REQ = json_decode($HTTP_RAW_POST_DATA, true);
// print_r($_REQ);

//响应请求返回的数据
$_RES = [
    'odr' => $_REQ['odr'],
    'errCode' => 0,
    'msg' => 'Success.',
    'data' => [],
    'PHPSESSID' => $sessid
];


/**
 * 登录,否则检查是否已登录
 * odr
 * code
 * 
 */
if ($_REQ['odr'] == 'userLogin') {

    // print_r($_REQ);
    $appid = 'wxe20d16baf3de2c93';
    $secret = '58172eee2a9ef3cbeccab2def3d6f1ff';
    $js_code = $_REQ['code'];
    $grant_type = $_REQ['grant_type'];


    $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$secret}&js_code={$js_code}&grant_type={$grant_type}";

    // echo $url;

    $result = json_decode(file_get_contents($url), true);

    /*********************************************************/
    /*********************** DEBUG专用 ************************/
    if (isset($_REQ['mode'])) {
        if ($_REQ['mode'] == 'debug') {

            $result['openid'] = 'opIPT5NpFdxQfQ3IDIc_oFPyPSqc';
        }
    }
    /*********************************************************/

    $_RES['data']['wx_msg'] = $result;

    if (isset($result['openid'])) {

        $_RES['data']['openid'] = $result['openid'];

        //将openid写入session
        $_SESSION['openid'] = $result['openid'];

        //检测数据库中是否已有此openid
        if ($db->has('scrtmgr_userinfo', [
            'AND' => [
                'open_id' => $result['openid'],
                'enable' => 1
            ]
        ])) {
            //将user_id与level写入session
            $db_res = $db->get('scrtmgr_userinfo', [
                'user_id',
                'level'
            ], [
                'AND' => [
                    'open_id' => $result['openid'],
                    'enable' => 1
                ]
            ]);

            $_SESSION['user_id'] = $db_res['user_id'];
            $_SESSION['level'] = $db_res['level'];
            $_SESSION['Auth'] = true;

            $_RES['data']['user_id'] = $db_res['user_id'];
            $_RES['data']['level'] = $db_res['level'];
            $_RES['msg'] = '登录成功';
        } else {
            $_SESSION['Auth'] = false;
            $_RES['errCode'] = 4002;
            $_RES['msg'] = '登录成功，请注册';
        }
    } else {
        $_RES['errCode'] = 4003;
        $_RES['msg'] = '获取openid失败';
    }
} else {

    if (!isset($_SESSION["openid"])) {
        $_RES['errCode'] = 4001;
        $_RES['msg'] = '请先登录';
        die(json_encode($_RES, JSON_UNESCAPED_UNICODE));
    } else if (!$_SESSION['Auth'] && $_REQ['odr'] != 'userAuth') {
        //未认证并且调用的非验证接口
        $_RES['errCode'] = 4004;
        $_RES['msg'] = '请先注册';
        die(json_encode($_RES, JSON_UNESCAPED_UNICODE));
    }
}

// $level = $db->select('scrtmgr_userinfo','level')

// print($_REQ['odr']=='userAuth');

switch ($_REQ['odr']) {
    case 'userAuth':
        userAuth();
        // print("fasfa");
        break;
    case 'userBasicInfoAdd':
        userBasicInfoAdd();
        break;
    case 'userBasicInfoGet':
        userBasicInfoGet();
        break;
    case 'locationAdd':
        locationAdd();
        break;
    case 'locationGet':
        locationGet();
        break;
    case 'groupAdd':
        groupAdd();
        break;
    case 'groupUserAdd':
        groupUserAdd();
        break;
    case 'attendanceTaskAdd':
        attendanceTaskAdd();
        break;
    case 'attendanceTaskGet':
        attendanceTaskGet();
        break;
    case 'attendanceTaskInfoAdd':
        attendanceTaskInfoAdd();
        break;
    case 'attendanceTaskInfoGet':
        attendanceTaskInfoGet();
        break;
    case 'attendanceHistoryAdd':
        attendanceHistoryAdd();
        break;
    case 'attendanceHistoryGet':
        attendanceHistoryGet();
        break;
    case 'userAllBasicInfoGet':
        userAllBasicInfoGet();
    default:
        break;
}





print(json_encode($_RES, JSON_UNESCAPED_UNICODE));
